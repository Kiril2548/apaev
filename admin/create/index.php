<?php 
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/layouts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/partials/admin.php');

echo $header;

?>
<div class="container-fluid mt-2">
	<div class="row full">
<?php
echo $sidebar_admin;

if($_GET['type'] == 'category'){

	echo $category_form;

} elseif ($_GET['type'] == 'prod') {

	echo $flowers_form;

} elseif ($_GET['type'] == 'order') {



} elseif ($_GET['type'] == 'news'){

	echo $news_form;

} elseif ($_GET['type'] == 'gallery'){

	echo $gallery_form;

} elseif ($_GET['type'] == 'service'){

	echo $service_form;

} elseif ($_GET['type'] == 'bonus'){

	echo $bonus_form;

} elseif ($_GET['type'] == 'phone'){

	echo $phone_form;

} elseif ($_GET['type'] == 'email'){

	echo $email_form;

}else {
?>
	<script>
		window.history.back();
	</script>
<?php
}

?>
	</div>
</div>
<?=$footer_admin ?>