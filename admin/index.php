<?php error_reporting(0); ?>
<?php
session_start();

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/php/layouts.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/partials/admin.php');
		

	echo $header;

?>

<style>

	ol{

		display: flex;

		justify-content: flex-end;

	}
	
</style>

<div class="container-fluid mt-2">

	<div class="row full">

		<?php if($_SESSION['username'] =="madinelli87" && $_SESSION['pass'] =="madinelli87"){ ?>

		<?=$sidebar_admin?>

		<div class="col-md-9">

			<?php

			    

				if($_GET['type'] == 'main') {

			?>

			<div class="container-fluid sidebar">

				        <div class="container">

				            <div class="row">

				                <div class="col">

				                    <p class="text-center" style="font-family: Palatino Linotype;">Главная страница панели Администратора</p>

				                    <p class="text-teft" style="font-family: Palatino Linotype;">Вы зашли как: Мадина Апаева</p>

				                    <p class="text-teft" style="font-family: Palatino Linotype; ">Информация:</p>

				                    <p class="text-teft" style="font-family: Palatino Linotype;">Для начала работы с панелью Администратора воспользуйтесь боковым меню</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype; font-size:"><i class="fa fa-arrow-left" aria-hidden="true"></i></p>

				                     <p class="text-teft" style="font-family: Palatino Linotype; font-size: ">- Вкладка Категории даёт возможность контролировать вывод типов Ваших товаров.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Цветы даёт возможность контролировать товары выбранного вами типа.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Новости отвечает за информирование на сайте в вкладке Новости.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Галерея осуществляет контроль вывода изображений (контента) на главную страницу сайта.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Услуги аналогична вкладке Цветы.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Акции даёт возможность контролировать информацию об проводимых Акциях.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Заказы отображает информацию о пользователях и товаре,который они успешно заказали.</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Телефон отвечает за вывод контактных данных на главной странице сайта</p>

				                     <p class="text-teft" style="font-family: Palatino Linotype;">- Вкладка Почта контролирует поступление писем о Заказах на указанные Вами почты</p>

				                </div>

				            </div>

				        </div>

				    </div>

			<?php

			} elseif($_GET['type'] == 'prod') {

			?>

			<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=prod&action=create" class="btn btn-outline-success btn-sm">Добавить Цветы</a>

				</ol>

			</nav>

			<?php

			echo $prods_table;

			} elseif($_GET['type'] == 'order') {

				echo $orders_table;

			?>

			<?php

			}  elseif($_GET['type'] == 'news') { 

				?>

				<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=news&action=create" class="btn btn-outline-success btn-sm">Добавить Новость</a>

				</ol>

			</nav>

			<?php

			echo $news_table;

			} elseif($_GET['type'] == 'gallery') { 

				?>

				<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=gallery&action=create" class="btn btn-outline-success btn-sm">Обновить Галерею</a>

				</ol>

			</nav>

			<?php

			echo $gallery_table;

			} elseif($_GET['type'] == 'service') { 

				?>

				<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=service&action=create" class="btn btn-outline-success btn-sm">Добавить Услугу</a>

				</ol>

			</nav>

			<?php

			echo $service_table;

			} elseif($_GET['type'] == 'bonus') { 

				?>

				<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=bonus&action=create" class="btn btn-outline-success btn-sm">Добавить Акции</a>

				</ol>

			</nav>

			<?php

			echo $bonus_table;

			} elseif($_GET['type'] == 'phone') { 

				?>

				<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=phone&action=create" class="btn btn-outline-success btn-sm">Добавить Телефон</a>

				</ol>

			</nav>

			<?php

			echo $phone_table;

			} elseif($_GET['type'] == 'email') { 

				?>

				<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=email&action=create" class="btn btn-outline-success btn-sm">Добавить Почту</a>

				</ol>

			</nav>

			<?php

			echo $email_table;

			} elseif($_GET['type'] == 'reviews') { 

				?>

			<?php

			echo $reviews_table;

			} elseif($_GET['type'] == 'category') { 

				?>

				 	<nav aria-label="breadcrumb">

				<ol class="breadcrumb">

					<a href="<?= $site_url ?>/admin/create?type=category&action=create" class="btn btn-outline-success btn-sm">Добавить Категорию</a>

				</ol>

			</nav>   

			<?php

			echo $categories_table;

			} elseif($_GET['type'] == 'exit') {

			    echo $exit_table;

			    session_destroy();

	            echo '<script>window.location = "http://tsvetkoff-uk.site/admin/";

            </script>';

	            }

			?>

		</div>

		<?php

		}   else { ?>

		<div class="col-md-1"></div>

		<div class="col-md-10">

			<h1>Авторизация</h1>

			<form class="form-group" action="auth.php" method="POST">

				<br>

				<input type="text" placeholder="Login" name="log" class="form-control" required="">

				<br>

				<input type="password" placeholder="Pass" name="pass" class="form-control" required="">

				<br>

				<input class="btn btn-outline-success btn-block" type="submit" value="Войти">

			</form>

		</div>

		<?php

		}

		?>

	</div>

</div>

<?=$footer_admin ?>