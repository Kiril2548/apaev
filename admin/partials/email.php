<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
$tr = "";
$res = getAll('email');
$i = 1;

while($row = mysqli_fetch_assoc($res)){
	$tr .= '
	<tr>
		<td> '. $i .' </td>
		<td> '. $row['email'] .' </td>
		<td class="text-left">
					<div class="btn-group">
								<a href="'. $site_url .'/admin/edit?type=email&action=edit&email='. $row['id'] .'" class="btn btn-outline-info btn-sm">Изменить</a>
								<a href="'. $site_url .'/admin/delete?type=email&id='. $row['id'] .'" class="btn btn-outline-danger btn-sm">Удалить</a>
					</div>
		</td>
	</tr>
	';
	$i++;
}

$email_table = '
	<table class="table">
					<thead>
									<tr>
														<th>#</th>
														<th>Почта</th>
														<th>Действие</th>
									</tr>
					</thead>
					<tbody>
								'. $tr .'
					</tbody>
	</table>
';