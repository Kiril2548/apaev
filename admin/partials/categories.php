<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
$tr = "";
$res = getAll('categories');

while($row = mysqli_fetch_assoc($res)){
	$tr .= '
	<tr>
		<td> '. $row['id'] .' </td>
		<td> '. $row['title'] .' </td>
		<td class="text-left">
					<div class="btn-group">
								<a href="'. $site_url .'/admin/edit?type=category&action=edit&category='. $row['id'] .'" class="btn btn-outline-info btn-sm">Изменить</a>
								<a href="'. $site_url .'/admin/delete?type=category&id='. $row['id'] .'" class="btn btn-outline-danger btn-sm">Удалить</a>
					</div>
		</td>
	</tr>
	';
}

$categories_table = '
	<table class="table">
					<thead>
									<tr>
														<th>#</th>
														<th>Наименование</th>
														<th>Действие</th>
									</tr>
					</thead>
					<tbody>
								'. $tr .'
					</tbody>
	</table>
';