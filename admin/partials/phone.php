<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
$tr = "";
$res = getAll('phone');
$i = 1;

while($row = mysqli_fetch_assoc($res)){
	$tr .= '
	<tr>
		<td> '. $i .' </td>
		<td> '. $row['phone'] .' </td>
		<td class="text-left">
					<div class="btn-group">
								<a href="'. $site_url .'/admin/edit?type=phone&action=edit&phone='. $row['id'] .'" class="btn btn-outline-info btn-sm">Изменить</a>
								<a href="'. $site_url .'/admin/delete?type=phone&id='. $row['id'] .'" class="btn btn-outline-danger btn-sm">Удалить</a>
					</div>
		</td>
	</tr>
	';
	$i++;
}

$phone_table = '
	<table class="table">
					<thead>
									<tr>
														<th>#</th>
														<th>Номер Телефона</th>
														<th>Действие</th>
									</tr>
					</thead>
					<tbody>
								'. $tr .'
					</tbody>
	</table>
';