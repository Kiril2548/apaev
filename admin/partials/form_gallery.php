<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/layouts.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');

if($_GET['action'] == "create" && $_GET['type'] == "gallery"){
$gallery_form = '
<div class="col-md-9">
		<div class="card">
			<div class="card-header">
	Обновление Галереи
	</div>
		<div class="card-body">
			<form action="'.$site_url.'/admin/store/index.php" enctype="multipart/form-data" method="post">
					<input type="hidden" name="type" value="gallery">
					<input type="hidden" name="action" value="store">
					<div class="input-group mb-3">
							<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroup-sizing-default">Изображение</span>
							</div>
							<input name="img" type="file" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
					</div>
						<button type="submit" class="btn btn-outline-dark btn-sm btn-block">Добавить</button>
			</form>
			</div>
		</div>
		</div>
';
} elseif($_GET['action'] == "edit" && $_GET['type'] == "gallery") {

	$product = $_GET['gallery'];
	$product = getById('gallery', $product);
	$product = mysqli_fetch_assoc($product);
	$gallery_form ='
<div class="col-md-9">
		<div class="card">
			<div class="card-header">
	Редактирование Галереи
	</div>
				<div class="card-body">
			<form action="'.$site_url.'/admin/store/index.php" enctype="multipart/form-data" method="post">
					<input type="hidden" name="type" value="gallery">
					<input type="hidden" name="action" value="update">
					<input type="hidden" name="id" value="'. $product['id'] .'">
					<div class="input-group mb-3">
							<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroup-sizing-default">Изображение</span>
							</div>
							<input name="img" type="file" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="'. $product['img'] .'">
					</div>
					<img src="'. $product['img'] .'" class="mb-2" height="100px;" alt="">
						<button type="submit" class="btn btn-outline-dark btn-sm btn-block">Сохранить</button>
			</form>
			</div>
		</div>
		</div>
';
} else {
	$gallery_form = '';
}