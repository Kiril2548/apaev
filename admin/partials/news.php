<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
$tr = "";
$res = getAll('news');

while($row = mysqli_fetch_assoc($res)){
	$tr .= '									
	<tr>
		<td> '. $row['id'] .' </td>
		<td><a href="'. $row['img'] .'" target="_blank"><img src="'. $row['img'] .'" height="30px"></a></td>
		<td> '. $row['title'] .' </td>
		<td style="word-break: break-all;"> '. $row['description'] .' </td>
		<td class="text-left">
					<div class="btn-group">
								<a href="'. $site_url .'/admin/edit?type=news&action=edit&news='. $row['id'] .'" class="btn btn-outline-info btn-sm">Изменить</a>
								<a href="'. $site_url .'/admin/delete?type=news&id='. $row['id'] .'" class="btn btn-outline-danger btn-sm">Удалить</a>
					</div>
		</td>
	</tr>
	';
}

$news_table = '
	<table class="table">
					<thead>
									<tr>
														<th>#</th>
														<th>Изображение</th>
														<th>Заголовок</th>
														<th>Информация</th>
														<th>Действие</th>
									</tr>
					</thead>
					<tbody>
								'. $tr .'
					</tbody>
	</table>
';

