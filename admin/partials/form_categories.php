<?php require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');

if($_GET['action'] == "create" && $_GET['type'] == "category"){

	$category_form = '
	<div class="col-md-9">
		<div class="card">
		<div class="card-header">
		Добавление категории
		</div>
			<div class="card-body">
					<form action="'.$site_url.'/admin/store/index.php" method="post">
							<input type="hidden" name="type" value="category">
							<input type="hidden" name="action" value="store">
								<div class="input-group mb-3">
									<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-default">Название категории</span>
									</div>
									<input autofocus name="title" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
							</div>
									<button class="btn btn-outline-dark btn-sm btn-block" type="submit">Добавить</button>
					</form>
				</div>
			</div>
	</div>
	';

} elseif($_GET['action'] == "edit" && $_GET['type'] == "category") {

	$category = $_GET['category'];
	$category = getById('categories', $category);
	$category = mysqli_fetch_assoc($category);
		$category_form = '
		<div class="col-md-9">
			<div class="card">
			<div class="card-header">
			Изменение категории
			</div>
				<div class="card-body">
						<form action="'.$site_url.'/admin/store/index.php" method="post">
							<input type="hidden" name="type" value="category">
							<input type="hidden" name="action" value="update">
							<input type="hidden" name="id" value="'. $category['id'] .'">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroup-sizing-default">Название категории</span>
								</div>
								<input autofocus name="title" value="'. $category['title'] .'" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
							</div>
							<button class="btn btn-outline-dark btn-sm btn-block" type="submit">Сохранить</button>
						</form>
					</div>
				</div>
		</div>
		';
} else {
	$category_form = '';
}