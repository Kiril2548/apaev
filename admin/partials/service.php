<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
$tr = "";
$res = getAll('service');
$i = 1;

while($row = mysqli_fetch_assoc($res)){
	$tr .= '									
	<tr>
		<td> '. $i .' </td>
		<td> '. $row['title'] .' </td>
		<td style="word-break: break-all;"> '. $row['description'] .' </td>
		<td> '. $row['price'] .' </td>
		<td class="text-left">
					<div class="btn-group">
								<a href="'. $site_url .'/admin/edit?type=service&action=edit&service='. $row['id'] .'" class="btn btn-outline-info btn-sm">Изменить</a>
								<a href="'. $site_url .'/admin/delete?type=service&id='. $row['id'] .'" class="btn btn-outline-danger btn-sm">Удалить</a>
					</div>
		</td>
	</tr>
	';
	$i++;
}

$service_table = '
	<table class="table">
					<thead>
									<tr>
														<th>#</th>
														<th>Название</th>
														<th>Описание</th>
														<th>Цена</th>
														<th>Действие</th>
									</tr>
					</thead>
					<tbody>
								'. $tr .'
					</tbody>
	</table>
';

