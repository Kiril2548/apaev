<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');
$tr = "";
$res = getAll('gallery');

while($row = mysqli_fetch_assoc($res)){
	$tr .= '									
	<tr>
		<td> '. $row['id'] .' </td>
		<td><a href="'. $row['img'] .'" target="_blank"><img src="'. $row['img'] .'" height="30px"></a></td>
		<td class="text-left">
					<div class="btn-group">
								<a href="'. $site_url .'/admin/edit?type=gallery&action=edit&gallery='. $row['id'] .'" class="btn btn-outline-info btn-sm">Изменить</a>
								<a href="'. $site_url .'/admin/delete?type=gallery&id='. $row['id'] .'" class="btn btn-outline-danger btn-sm">Удалить</a>
					</div>
		</td>
	</tr>
	';
}

$gallery_table = '
	<table class="table">
					<thead>
									<tr>
														<th>#</th>
														<th>Изображение</th>
														<th>Действие</th>
									</tr>
					</thead>
					<tbody>
								'. $tr .'
					</tbody>
	</table>
';

