<?php require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');



if($_GET['action'] == "create" && $_GET['type'] == "service"){



	$service_form = '

	<div class="col-md-9">

		<div class="card">

		<div class="card-header">

		Добавление Услуги

		</div>

			<div class="card-body">

					<form action="'.$site_url.'/admin/store/index.php" method="post">

							<input type="hidden" name="type" value="service">

							<input type="hidden" name="action" value="store">

								<div class="input-group mb-3">

									<div class="input-group-prepend">

											<span class="input-group-text" id="inputGroup-sizing-default">Наименование Услуги</span>

									</div>

									<input autofocus name="title" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>

							</div>

							<span class="input-group-text" id="inputGroup-sizing-default">Описание</span>

					<br>

					<div class="input-group mb-3">

							<div class="input-group-prepend">

							</div>

							<textarea rows="5" name="description" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="'. $product['description'] .'">'. $product['description'] .'</textarea>

					</div>

							<div class="input-group mb-3">

									<div class="input-group-prepend">

											<span class="input-group-text" id="inputGroup-sizing-default">Цена</span>

									</div>

									<input autofocus name="price" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>

							</div>

									<button class="btn btn-outline-dark btn-sm btn-block" type="submit">Добавить</button>

					</form>

				</div>

			</div>

	</div>

	';



} elseif($_GET['action'] == "edit" && $_GET['type'] == "service") {



	$category = $_GET['service'];

	$category = getById('service', $category);

	$category = mysqli_fetch_assoc($category);

		$service_form = '

		<div class="col-md-9">

			<div class="card">

			<div class="card-header">

			Изменение услуги

			</div>

				<div class="card-body">

						<form action="'.$site_url.'/admin/store/index.php" method="post">

							<input type="hidden" name="type" value="service">

							<input type="hidden" name="action" value="update">

							<input type="hidden" name="id" value="'. $category['id'] .'">

							<div class="input-group mb-3">

								<div class="input-group-prepend">

									<span class="input-group-text" id="inputGroup-sizing-default">Наименование Услуги</span>

								</div>

								<input autofocus name="title" value="'. $category['title'] .'" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>

							</div>

						<span class="input-group-text" id="inputGroup-sizing-default">Описание</span>

					<br>

					<div class="input-group mb-3">

							<div class="input-group-prepend">

							</div>

							<textarea rows="5" name="description" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="'. $category['description'] .'">'. $category['description'] .'</textarea>

					</div>

							<div class="input-group mb-3">

								<div class="input-group-prepend">

									<span class="input-group-text" id="inputGroup-sizing-default">Цена</span>

								</div>

								<input autofocus name="price" value="'. $category['price'] .'" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>

							</div>

							<button class="btn btn-outline-dark btn-sm btn-block" type="submit">Сохранить</button>

						</form>

					</div>

				</div>

		</div>

		';

} else {

	$service_form = '';

}