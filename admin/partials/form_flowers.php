<?php


error_reporting(0);


require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');


require_once($_SERVER['DOCUMENT_ROOT'].'/php/layouts.php');


require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');


$options = '';


$categories = getAll('categories');


while($category = mysqli_fetch_assoc($categories)){


	$option .= '<option value="'. $category['id'] .'">'. $category['title'] .'</option>';


}





if($_GET['action'] == "create" && $_GET['type'] == "prod"){


$flowers_form = '


<div class="col-md-9">


		<div class="card">


			<div class="card-header">


	Добавление цветов


	</div>


				<div class="card-body">


			<form action="'.$site_url.'/admin/store/index.php" enctype="multipart/form-data" method="post">


					<input type="hidden" name="type" value="prod">


					<input type="hidden" name="action" value="store">


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Категория</span>


							</div>


							<select name="category_id" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>


									'. $option .'


							</select>


					</div>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Фото</span>


							</div>


							<input name="img" type="file" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>


					</div>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Название</span>


							</div>


							<input name="title" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>


					</div>


						<span class="input-group-text" id="inputGroup-sizing-default">Описание</span>


						<br>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


							</div>


							<textarea rows="5" name="description" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required wrap="soft"></textarea>


					</div>

					<span class="input-group-text" id="inputGroup-sizing-default">Информация</span>


						<br>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


							</div>

<textarea rows="5" name="info" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"></textarea>


					</div>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Цена</span>


							</div>


							<input name="price" type="number" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>


					</div>


						<button type="submit" class="btn btn-outline-dark btn-sm btn-block">Добавить</button>


			</form>


			</div>


		</div>


		</div>


';


} elseif($_GET['action'] == "edit" && $_GET['type'] == "prod") {





	$product = $_GET['prod'];


	$product = getById('products', $product);


	$product = mysqli_fetch_assoc($product);


	$flowers_form ='


<div class="col-md-9">


		<div class="card">


			<div class="card-header">


	Редактирование цветов


	</div>


				<div class="card-body">


			<form action="'.$site_url.'/admin/store/index.php" enctype="multipart/form-data" method="post">


					<input type="hidden" name="type" value="prod">


					<input type="hidden" name="action" value="update">


					<input type="hidden" name="id" value="'. $product['id'] .'">


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Категория</span>


							</div>


							<select id="select_edit" name="category_id" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>


									'. $option .'


							</select>


							<script>


								document.getElementById("select_edit").value = '. $product['category_id'] .'


							</script>


					</div>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Фото</span>


							</div>


							<input name="img" type="file" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="'. $product['img'] .'">


					</div>


					<img src="'. $product['img'] .'" class="mb-2" height="100px;" alt="">


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Название</span>


							</div>


							<input name="title" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="'. $product['title'] .'">


					</div>


					<span class="input-group-text" id="inputGroup-sizing-default">Описание</span>


					<br>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


							</div>


							<textarea rows="5" name="description" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="'. $product['description'] .'">'. $product['description'] .'</textarea>


					</div>

					<span class="input-group-text" id="inputGroup-sizing-default">Информация</span>


					<br>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


							</div>


<textarea rows="5" name="info" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"  value="'. $product['info'] .'">'. $product['info'] .'</textarea>


					</div>


					<div class="input-group mb-3">


							<div class="input-group-prepend">


									<span class="input-group-text" id="inputGroup-sizing-default">Цена</span>


							</div>


							<input name="price" type="number" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="'. $product['price'] .'">


					</div>


						<button type="submit" class="btn btn-outline-dark btn-sm btn-block">Сохранить</button>


			</form>


			</div>


		</div>


		</div>


';


} else {


	$flowers_form = '';


}