<?php

require_once('connection.php');

function getAll($table){
    return mysqli_query(connection(), "SELECT * FROM $table");
}

function hasMany($table, $parent_id){
    //return mysqli_query(connection(), "SELECT * FROM $table WHERE");
}

function getById($table, $id){
    return mysqli_query(connection(), "SELECT * FROM $table WHERE id=$id");
}

function belongTo($table, $revelation_table, $table_id){
    return mysqli_query(connection(), "SELECT * FROM $table WHERE $revelation_table=$table_id");
}

function update($table, $id, ...$data){
	$params = arrayToString($data);	
    return mysqli_query(connection(), "UPDATE $table SET $params WHERE id=$id");
}

function insert($table, ...$data){ // $data -> ['ключ' => 'значение', ....]
   $sql  = "INSERT INTO $table";
   $sql .= " (`".implode("`, `", array_keys($data[0]))."`)";
   $sql .= " VALUES ('".implode("', '", $data[0])."') ";
	
    return mysqli_query(connection(), $sql);
}

function delete($table, $id){
	return mysqli_query(connection(), "DELETE FROM $table WHERE id=$id");
}

function deleteAllProductsWithCategory($table, $id){
	return mysqli_query(connection(), "DELETE FROM $table WHERE category_id=$id");
}







/* Dop function */
function arrayToString($data){
	$string = "";
	$i = 1;
	foreach($data[0] as $key => $value){
		if($i == count($data[0])) {
			$string .= "$key='$value'";
		} else {
			$string .= "$key='$value',";
		}
		$i++;
	}
	return $string;
}