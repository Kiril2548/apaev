<?php error_reporting(0); ?>
<style>
    	@media (min-width: 1920px) { 
	    
	    .sidebar *{
	        font-size:35px;
	    }
	}


     @media (width: 1600px) { 
      
      .sidebar *{
          font-size:20px;
      }
  }
</style>
<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');

$tr = "";

$res = getAll('orders');

$i =0;

while($row = mysqli_fetch_assoc($res)){

        $i=$i+1;

}

	$sidebar_admin = '	

	<div class="col-md-3 sidebar">

        <div class="list-group">

            <a href="'. $site_url .'/admin/?type=main" class="list-group-item bg-secondary text-light text-center">

                <h5>Админ панель</h5>

            </a>

             <a href="'. $site_url .'/admin?type=main" class="list-group-item">

                <i class="fa fa-home" aria-hidden="true"></i> Главная

            </a>

            <a href="'. $site_url .'/admin?type=category" class="list-group-item">

                <i class="fa fa-list" aria-hidden="true"></i> Категории

            </a>

            <a href="'. $site_url .'/admin?type=prod" class="list-group-item">

               <i class="fa fa-sitemap" aria-hidden="true"></i> Цветы

            </a>

            <a href="'. $site_url .'/admin?type=news" class="list-group-item">

               <i class="fa fa-newspaper-o" aria-hidden="true"></i> Новости

            </a>

             <a href="'. $site_url .'/admin?type=gallery" class="list-group-item">

                <i class="fa fa-th-large" aria-hidden="true"></i> Галерея

            </a>

             <a href="'. $site_url .'/admin?type=service" class="list-group-item">

               <i class="fa fa-handshake-o" aria-hidden="true"></i> Услуги

            </a>

             <a href="'. $site_url .'/admin?type=bonus" class="list-group-item">

                <i class="fa fa-money" aria-hidden="true"></i> Акции

            </a>

            <a href="'. $site_url .'/admin?type=order" class="list-group-item">

                <i class="fa fa-shopping-cart"></i> Заказы ['.$i.']

            </a>

            <a href="'. $site_url .'/admin?type=phone" class="list-group-item">

               <i class="fa fa-mobile" aria-hidden="true"></i> Телефон

            </a>

            <a href="'. $site_url .'/admin?type=email" class="list-group-item">

              <i class="fa fa-envelope-o" aria-hidden="true"></i> Почта

            </a>
            
            <a href="'. $site_url .'/admin?type=reviews" class="list-group-item">

             <i class="fa fa-commenting-o" aria-hidden="true"></i> Отзывы

            </a>

            <a href="'. $site_url .'/admin?type=exit" class="list-group-item">

                <i class="fa fa-sign-out" aria-hidden="true"></i>Выход

            </a>

        </div>   

    </div>   

';