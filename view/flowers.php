<?php session_start();?>
<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/layouts.php');

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');

	

	$category = $_GET['category'];

	

	$products = belongTo('products', 'category_id', $category);



	$category = getById('categories', $category);

	$category = mysqli_fetch_assoc($category);

	$carts = array();

	$items = array();



	echo $header;

?>
<?= $sidebar ?>

<style>

.update:hover{

	text-decoration: none;

}



.update{

	color:black;

	font-family: Lucida Sans Unicode;

	margin-left: 14px;

	display: inline;

	list-style-type: none;

	font-size: 24px;

	text-decoration: none;

}

#fix-img{
    height: 0;
padding-top: 120%;
position: relative;
display: block;
}

.card-img-top{
position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
max-height: 100%;
max-width: 100%;
}

#products{
	font-size: 25px;
	font-family: Palatino Linotype;
}

	.price{
		font-size: 16px;
	}
	
	#button-product{
		font-size: 16px;
	}
	
	#product-title{
	font-size: 20px;
	}

	@media (min-width: 1920px) {
	    #products{
	        font-size: 45px;
	        font-family: Palatino Linotype;
        }
        
        	.price{
		font-size: 32px;
	}
	
	#button-product{
		font-size: 35px;
		margin: 5px;
	}
	
	#product-title{
	font-size: 35px;
	font-weight: 600;
	word-wrap: break-word; 
	font-family: Times New Roman;
	}
	    
	}

	@media (width: 1600px) {
	    #products{
	        font-size: 30px;
	        font-family: Palatino Linotype;
        }
        
    .price{
		font-size: 20px;
	}
	
	#button-product{
		font-size: 20px;
		margin: 3px;
	}
	
	#product-title{
	font-size: 24px;
	font-weight: 600;
	word-wrap: break-word; 
	font-family: Times New Roman;
	}
	    
	}
	
</style>

<div class="container-fluid">

	<div class="row justify-content-center">

		<div class="col-lg-9" style="overflow-x: hidden;">

			<p class="info" id="products"><?= $category['title'] ?></p>

			<div class="row">

				<?php

					while ($product = mysqli_fetch_assoc($products)) {

						$b=''. $site_url .'/view/flower.php?product='. $product['id'] .'';

				?>

				<div class="col-md-4 mb-5">
				    
					<div id="fix-img"><a href="<?= $product['img'] ?>" target=_blank><img class="card-img-top item"  height="95%" src="<?= $product['img'] ?>" alt="Card image cap"></a></div>
				    
					<div class="card-block text-center">

				<span class="card-title" id="product-title" style="word-wrap: break-word; font-family: Times New Roman;"><?= $product['title'] ?></span>
				<br>
				<span class="price">Цена - <?= $product['price'] ?> Тенге</span>

				<form method="POST">

					<input type="hidden" name="id" value="<?= $product['id'] ?>">

					<input type="submit" name="next"  class="btn btn-outline-success"  id="button-product" value="В Корзину"></a>

							<a href="<?= $b ?>" class="btn btn-outline-success" id="button-product">Заказать</a></form>

						</div>

					</div>

				<?php

					}

				?>

			</div>

		</div>

	</div>

</div>

</div>
	<div id="spliter" style = "height: 30%;">

</div>
<?= $footer ?>

</div>

<?php

if(empty($_SESSION['items'])){

	array_push($items, "carts");

	$_SESSION['items'] = $items;

}



if(isset($_POST['next'])){

 	$id = $_POST['id'];

 	$product = $_GET['products'];

 	$product = getById('products', $id);

	$product = mysqli_fetch_assoc($product);

	unset($carts);

	$carts = array();

	array_push($carts,$product['id'],$product['img'], $product['title'],$product['price']);



	if(!empty($_SESSION['items'])){

		array_push($items, $carts);

		if(!in_array($items, $_SESSION['items'])) {

			array_push($_SESSION['items'], $items);

			}



	} else {

		array_push($items, "carts");

		$_SESSION['items'] = $items;

	}

	echo "<script>window.location.href='flowers.php?category=". $category['id'] ."'</script>";



}

?>
