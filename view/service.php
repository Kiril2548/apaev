<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/const.php');

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/layouts.php');

	require_once($_SERVER['DOCUMENT_ROOT'].'/php/db.php');



	echo $header;

	

	$service = getAll('service');

	$mass = array();

	while ($row = mysqli_fetch_assoc($service)) {

		${'array_'.$row['id']} = array($row['title'],$row['description'],$row['price'],$row['id']);

		array_push($mass, ${'array_'.$row['id']});

	}

	?>

<div id="contr">

<?=$sidebar ?>

<style type="">
	#ser{
		color: red;
	}

.info{
	font-family: Palatino Linotype; 
	font-size: 25px;
}

.title-service{
	font-family: Palatino Linotype; 
	font-size: 25px;
}

.info-service{
	font-family: Verdana; 
	font-size: 16px;
	 text-align: justify;
}

.info-price{
	font-family: Palatino Linotype; 
	font-size: 20px;
}
		@media (min-width: 1920px) {
			.info{
				font-family: Palatino Linotype; 
				font-size: 40px;
				font-weight: 600;
			}

			.title-service{
				font-family: Palatino Linotype; 
				font-size: 35px;
				font-weight: 600;
			}

			.info-service{
				font-family: Verdana; 
				font-size: 36px;
	 			text-align: justify;
			}

			.info-price{
				font-family: Palatino Linotype; 
				font-size: 40px;
			}
			
			.button{
			    font-size:35px;
			}
			
			
			.modal-fix-text{
				font-size: 30px;
			}

			.modal-fix{
				font-size: 25px;
			}

			.buttons{
				font-size: 25px;
			}

			.text-fix{
				font-size: 25px;
			}

		}


		@media (width: 1600px) {
			.info{
				font-family: Palatino Linotype; 
				font-size: 25px;
				font-weight: 600;
			}

			.title-service{
				font-family: Palatino Linotype; 
				font-size: 25px;
				font-weight: 600;
			}

			.info-service{
				font-family: Verdana; 
				font-size: 20px;
	 			text-align: justify;
			}

			.info-price{
				font-family: Palatino Linotype; 
				font-size: 20px;
			}
			
			.button{
			    font-size:20px;
			}
			
			
			.modal-fix-text{
				font-size: 20px;
			}

			.modal-fix{
				font-size: 20px;
			}

			.buttons{
				font-size: 20px;
			}

			.text-fix{
				font-size: 25px;
			}

		}
</style>

<div style = "position: relative;">

<div class="col-md-12">
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="col-md-12">
				<div class="col-md-12">
					<div class="col-md-12">

  		<p class="info">Услуги</p>
<div class="row" style="margin-top: 25px;">
<?php

if(count($mass) != 0){

for ($i = 0; $i < count($mass) ; $i++) {

 $title = $mass[$i][0];

 $description = $mass[$i][1];

 $price = $mass[$i][2];

?>

		<div class="col">

			<h4 class="title-service"><?= $title ?></h4>

			<p  class="info-service"><?= $description ?><hr></p>

			<p class="text-right info-price"><b class="info-price">Цена: <?=$price?> Тенге </b></p>

			<p class="text-right"><button class="btn btn-outline-dark button" data-toggle="modal" data-target="#exampleModal<?=$i?>">Заказать</button></p>



				<div class="modal fade" id="exampleModal<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

						<div class="modal-dialog" role="document">

							<div class="modal-content">

								<div class="modal-header">

									<h5 class="modal-title modal-fix-text" id="exampleModalLabel">Оформление Услуги</br>(<?= $title?>)</h5>

									<button type="button" class="close" data-dismiss="modal" aria-label="Close">

									<span aria-hidden="true">&times;</span>

									</button>

								</div>

								<div class="modal-body">

									<form id="form" action="service_buy.php" method="post">

										<div class="input-group mb-3">

											<div class="input-group-prepend">

												<span class="input-group-text modal-fix" id="inputGroup-sizing-default">Имя</span>

											</div>

											<input type="text" class="form-control text-fix" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="name" name="name" required>

										</div>

										<div class="input-group mb-3">

											<div class="input-group-prepend">

												<span class="input-group-text modal-fix" id="inputGroup-sizing-default">Фамилия</span>

											</div>

											<input type="text" class="form-control text-fix" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="family" name="family" required>

										</div>

										<div class="input-group mb-3">

											<div class="input-group-prepend">

												<span class="input-group-text modal-fix" id="phone">Телефон</span>

											</div>

<input type="text" class="form-control text-fix" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="phone_mask" name="phone" required>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="js/jquery.maskedinput.min.js"></script>

<script>

    $("#phone_mask").mask("+7(999)999-99-99");

</script>

										</div>

										<div class="input-group mb-3">

											<div class="input-group-prepend">

												<span class="input-group-text modal-fix" id="inputGroup-sizing-default">Адрес</span>

											</div>

											<input type="text" class="form-control text-fix" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="adress" name="adress" required>

										</div>

								<input type="hidden" name="summa" value="<?=$price?>">

								<input type="hidden" name="flower" value="<?=$title?>">

								<input type="hidden" name="order_id" value="<?= time() ?>">

										<h5 class="modal-fix-text">Сумма заказа: <?=$mass[$i][2]?> Тенге</h5>

									</div>

									<input type="hidden" name="del" value="3">

									<div class="modal-footer">

									<button type="button" class="btn btn-dange  buttons" data-dismiss="modal">Закрыть</button>

									<button type="submit"  class="btn btn-success  buttons">Заказать</button></form>

								</div>

							</div>

						</div>

					</div>

			</form>

		</div>

	</div>
		</div>


<?php

	}

?>
			</div>
				</div>
					</div>
						</div>
						</div>



 <?php

} else { ?>



	<div class="row">

					<div class="col-md-12">

					   <p class="text-center" style="font-family: Courier New; font-size: 20px; text-align: justify;">В данный момент никаких Услуг Мы не предоставляем.</p>

					</div>

				</div>

<?php

    }

?>

			</div>

	</div>

</div>

</div>

<div id="spliter" style = "height: 36.6%;">

</div>

<div>

<?=$footer ?>

</div>